import React       from 'react';
import { render }  from 'react-dom';
import { createStore, combineReducers } from 'redux';
import { Provider }                     from 'react-redux';
import * as reducers                    from '../shared/reducers';
import { applyMiddleware } from 'redux';
import promiseMiddleware   from '../shared/lib/promiseMiddleware';
import immutifyState   from '../shared/lib/immutifyState';
import { BrowserRouter } from 'react-router-dom';
import AgileTools from '../shared/components';
import { fromJS } from 'immutable';

const initialState = immutifyState(window.__INITIAL_STATE__);

const reducer = combineReducers(reducers);
const store   = applyMiddleware(promiseMiddleware)(createStore)(reducer, initialState);

render((
  <BrowserRouter>
    <Provider store={store}>
      <AgileTools/>
    </Provider>
  </BrowserRouter>
), document.getElementById('react-view'));