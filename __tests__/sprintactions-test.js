
import * as actions from '../shared/actions/sprintActions';
import {ADD_SPRINT, REMOVE_SPRINT, UPDATE_POINTS, UPDATE_TOTAL_POINTS, SAVE_BURNUP, GET_SPRINTS} from '../shared/constants';
describe('Sprint Actions', () => {
  it('should dispatch an action to add sprint', () => {
    const expectedAction = {
      type: ADD_SPRINT,
    };
    expect(
      actions.addSprint()
    ).toEqual(expectedAction);
  });
  it('should dispatch an action to remove sprint', () => {
    const expectedAction = {
      type: REMOVE_SPRINT,
    };
    expect(
      actions.removeSprint()
    ).toEqual(expectedAction);
  });
  it('should dispatch an action to update points with index and points', () => {
    const expectedAction = {
      type: UPDATE_POINTS,
      index: 1,
      value: 10
    };
    expect(
      actions.updatePoints(1,10)
    ).toEqual(expectedAction);
  });
   it('should dispatch an action to update total points with index and points', () => {
    const expectedAction = {
      type: UPDATE_TOTAL_POINTS,
      index: 1,
      value: 100
    };
    expect(
      actions.updateTotalPoints(1,100)
    ).toEqual(expectedAction);
  });
  // ... more tests here
});