import reducer from '../shared/reducers/sprintReducer'
import * as types from '../shared/constants'
import { fromJS } from 'immutable';

describe('sprint reducer', () => {
  it('should return the initial state', () => {
    expect(
      reducer(undefined, {})
    ).toEqual(
       fromJS({ sprints:[]})
    )
  })

  it('should handle ADD_SPRINT', () => {
    expect(
      reducer(  fromJS({ sprints:[]}), {
        type: types.ADD_SPRINT,
      })
    ).toEqual(
       fromJS({ sprints:[{'number':1, 'points': 0, 'totalPoints':0}]})
    )
  })
  it('should handle REMOVE_SPRINT', () => {
    expect(
      reducer(  
           fromJS({ sprints:[{'number':1, 'points': 0, 'totalPoints':0}]})
          , {
        type: types.REMOVE_SPRINT,
      })
    ).toEqual(
       fromJS({ sprints:[]})
    )
  })
   it('should handle UPDATE_POINTS', () => {
    expect(
      reducer(  
           fromJS({ sprints:[{'number':1, 'points': 0, 'totalPoints':0}]})
          , {
        type: types.UPDATE_POINTS,
        index: 0,
        value: 10
      })
    ).toEqual(
        fromJS({ sprints:[{'number':1, 'points': 10, 'totalPoints':0}]})
    )
  })
  it('should handle UPDATE_TOTAL_POINTS', () => {
    expect(
      reducer(  
           fromJS({ sprints:[{'number':1, 'points': 0, 'totalPoints':0}]})
          , {
        type: types.UPDATE_TOTAL_POINTS,
        index: 0,
        value: 100
      })
    ).toEqual(
        fromJS({ sprints:[{'number':1, 'points': 0, 'totalPoints':100}]})
    )
  })
  it('should handle SAVE_BURNUP', () => {
    expect(
      reducer(  
    null
          , {
        type: types.SAVE_BURNUP,
        result : {"data":{ sprints:[{'number':1, 'points': 10, 'totalPoints':100}]}}
      })
    ).toEqual(
        fromJS({ sprints:[{'number':1, 'points': 10, 'totalPoints':100}]})
    )
  })
  it('should handle GET_SPRINTS', () => {
    expect(
      reducer(  
    null
          , {
        type: types.GET_SPRINTS,
        result : {"data":{ sprints:[{'number':1, 'points': 10, 'totalPoints':100}]}}
      })
    ).toEqual(
        fromJS({ sprints:[{'number':1, 'points': 10, 'totalPoints':100}]})
    )
  })
  it('should handle GET_SPRINTS_FAILURE', () => {
    expect(
      reducer(  
    null
          , {
        type: types.GET_SPRINTS_FAILURE,
        result : {"data":{ sprints:[{'number':1, 'points': 10, 'totalPoints':100}]}}
      })
    ).toEqual(
        null
    )
  })
})