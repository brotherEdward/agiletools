import reducer from '../shared/reducers/viewReducer'
import * as types from '../shared/constants'
import { fromJS } from 'immutable';

describe('view reducer', () => {
  it('should return the initial state', () => {
    expect(
      reducer(undefined, {})
    ).toEqual(
      fromJS({showMenu:true})
    )
  })

  it('should handle SHOW_MENU', () => {
    expect(
      reducer(  fromJS({showMenu:false}), {
        type: types.SHOW_MENU,
      })
    ).toEqual(
      fromJS({showMenu:true})
    )
  })
   it('should handle HIDE_MENU', () => {
    expect(
      reducer(  fromJS({showMenu:true}), {
        type: types.HIDE_MENU,
      })
    ).toEqual(
      fromJS({showMenu:false})
    )
  })
  
})