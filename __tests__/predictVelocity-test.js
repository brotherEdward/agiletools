import predictVelocity from '../shared/lib/predictVelocity';

test('predict velocity', () => {
  expect(predictVelocity(3, 10, 30, 70, 1).totalSprintsRequired).toBe(10);
  expect(predictVelocity(3, 10, 30, 70, 1).factoredAverage).toBe(10);
});

test('predict velocity risk x2', () => {
  expect(predictVelocity(3, 10, 30, 70, 2).factoredAverage).toBe(5);
  expect(predictVelocity(3, 10, 30, 70, 2).totalSprintsRequired).toBe(17);
  
});


test('predict velocity average 0', () => {
  expect(predictVelocity(1, 1, 1, 10, 2).totalSprintsRequired).toBe(11);
  expect(predictVelocity(1, 1, 1, 10, 2).factoredAverage).toBe(1);
});