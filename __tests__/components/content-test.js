import React from 'react'
import { shallow } from 'enzyme'
import Content from '../../shared/components/Layout/Content'
import transCss from '../../shared/components/Layout/trans.css';


function setup() {
  const props = {
    hideMenu: jest.fn()
  }
  const enzymeWrapper = shallow(<Content {...props} >
    <div className="unique" />
  </Content>)
  return {
    props,
    enzymeWrapper
  }
}

describe('components', () => {
  describe('Content', () => {
    it('should render self and subcomponents ', () => {
      const { enzymeWrapper } = setup()
      expect(enzymeWrapper.find('#content-pusher').hasClass(transCss.stPusher)).toBe(true);
      expect(enzymeWrapper.find('#content-pusher').childAt(0).hasClass(transCss.stContent)).toBe(true);
      expect(enzymeWrapper.find('#content-pusher').childAt(0).childAt(0).hasClass(transCss.stContentInner)).toBe(true);

      expect(enzymeWrapper.find('#content-pusher').childAt(0).childAt(0).contains(<div className="unique" />)).toBe(true);
    })
    it('should call hide menu on content click', () => {
      const { enzymeWrapper, props } = setup()
      const input = enzymeWrapper.find('#content-pusher')
      input.props().onClick()
      expect(props.hideMenu.mock.calls.length).toBe(1)
    })
    
  })
})
