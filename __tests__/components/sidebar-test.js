import React from 'react'
import { shallow } from 'enzyme'
import SideBar from '../../shared/components/Layout/SideBar'
import transCss from '../../shared/components/Layout/trans.css';
import styles from './menu.css';

function setup() {
  const props = {
   showMenu: jest.fn(),
   save: jest.fn()
  }
  const enzymeWrapper = shallow(<SideBar {...props} >
    <div className="unique" />
  </SideBar>)
  return {
    props,
    enzymeWrapper
  }
}

describe('components', () => {
  describe('SideBar', () => {
    it('should render self and subcomponents ', () => {
      const { enzymeWrapper } = setup()
      expect(enzymeWrapper.find('#sidebar').hasClass(transCss.stMenu)).toBe(true);
      expect(enzymeWrapper.find('#sidebar').hasClass(transCss.stEffect)).toBe(true);
      expect(enzymeWrapper.find('#menu').hasClass(styles.menu2)).toBe(true);

      expect(enzymeWrapper.find('#save-btn').hasClass(styles.menuButton)).toBe(true);
      expect(enzymeWrapper.find('#save-btn').hasClass(styles.saveButton)).toBe(true);
      expect(enzymeWrapper.find('#show-menu-btn').hasClass(styles.menuButton)).toBe(true);
      expect(enzymeWrapper.contains(<div className="unique" />)).toBe(true);
    })
    it('should call save on save click', () => {
      const { enzymeWrapper, props } = setup()
      const input = enzymeWrapper.find('#save-btn')
      input.props().onClick()
      expect(props.save.mock.calls.length).toBe(1)
    })
    it('should call show menu on menu click', () => {
      const { enzymeWrapper, props } = setup()
      const input = enzymeWrapper.find('#show-menu-btn')
      input.props().onClick()
      expect(props.showMenu.mock.calls.length).toBe(1)
    })
    
  })
})
