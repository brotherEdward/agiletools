import React from 'react'
import { shallow } from 'enzyme'
import Wrap from '../../shared/components/Layout/Wrap'
import transCss from '../../shared/components/Layout/trans.css';
import styles from './wrap.css';

function setupMenuOpen() {
  const props = {
   title:"Title",
   menuOpen:true
  }
  const enzymeWrapper = shallow(<Wrap {...props} />)
  return {
    props,
    enzymeWrapper
  }
}
function setupMenuClosed() {
  const props = {
   title:"Title",
   menuOpen:false
  }
  const enzymeWrapper = shallow(<Wrap {...props} />)
  return {
    props,
    enzymeWrapper
  }
}

describe('components', () => {
  describe('Wrap', () => {
    it('should render self and subcomponents ', () => {
      const { enzymeWrapper } = setupMenuOpen()
      expect(enzymeWrapper.find('header').hasClass(styles.sectionHead)).toBe(true);
      expect(enzymeWrapper.find('header').text()).toBe('Title');
      expect(enzymeWrapper.find('#st-container').hasClass(transCss.stContainer)).toBe(true)
      expect(enzymeWrapper.find('#st-container').hasClass(transCss.stEffect)).toBe(true)
      
    })
    it('should have class for menu open', () => {
      const { enzymeWrapper } = setupMenuOpen()
      expect(enzymeWrapper.find('#st-container').hasClass(transCss.stMenuOpen)).toBe(true)
      
    })
    it('should have class for menu closed', () => {
      const { enzymeWrapper } = setupMenuClosed()
      expect(enzymeWrapper.find('#st-container').hasClass(transCss.stMenuOpen)).toBe(false)
    })

    it('renders children when passed in', () => {
      const wrapper = shallow(
        <Wrap>
          <div className="unique" />
        </Wrap>
      );
      expect(wrapper.find("#st-container").contains(<div className="unique" />)).toBe(true);
    });
})
 })
