import React from 'react'
import { shallow } from 'enzyme'
import Stat from '../../shared/components/Burnup/Stat'
import styles from '../../shared/components/Burnup/stat.css';


function setup() {
  const props = {
   value:99,
   label:"red balloons"
  }

  const enzymeWrapper = shallow(<Stat {...props} />)

  return {
    props,
    enzymeWrapper
  }
}

describe('components', () => {
  describe('Stat', () => {
    it('should render self and subcomponents', () => {
      const { enzymeWrapper } = setup()

      expect(enzymeWrapper.closest('div').hasClass(styles.stat)).toBe(true)
      expect(enzymeWrapper.closest('div').childAt(0).hasClass(styles.value)).toBe(true)
      expect(enzymeWrapper.closest('div').childAt(0).text()).toBe("99")
      expect(enzymeWrapper.closest('div').childAt(1).hasClass(styles.label)).toBe(true)
      expect(enzymeWrapper.closest('div').childAt(1).text()).toBe('red balloons')
    })

   
  })
})
