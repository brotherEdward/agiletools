import React from 'react'
import { shallow } from 'enzyme'
import AddSprint from '../../shared/components/Burnup/AddSprint'
import addImage from '../../../assets/addblk.png';
import styles from '../../shared/components/Burnup/addsprint.css';


function setup() {
  const props = {
    addSprint: jest.fn()
  }

  const enzymeWrapper = shallow(<AddSprint {...props} />)

  return {
    props,
    enzymeWrapper
  }
}

describe('components', () => {
  describe('AddSprint', () => {
    it('should render self and subcomponents', () => {
      const { enzymeWrapper } = setup()

      expect(enzymeWrapper.find('img').hasClass(styles.addSprintButton)).toBe(true)
    })

    it('should call add sprint on img click', () => {
      const { enzymeWrapper, props } = setup()
      const input = enzymeWrapper.find('img')
      input.props().onClick()
      expect(props.addSprint.mock.calls.length).toBe(1)
    })
  })
})
