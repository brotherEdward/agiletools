import express                   from 'express';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import methodOverride from 'method-override';
import shortid  from 'shortid';
import db from '../db';
import Burnup from '../model/burnup';

var router = express.Router();              // get an instance of the express Router
router.use(bodyParser.json());
// router.use(methodOverride(function(req, res){
//       if (req.body && typeof req.body === 'object' && '_method' in req.body) {
//         // look in urlencoded POST bodies and delete it
//         var method = req.body._method
//         delete req.body._method
//         return method
//       }
// }))


router.get(['/:id','/:id/:version'], function(req,res){
    //if doesnt exist then we should throw error resulting in 404.
    
    //if no version look up higest version
    var query = {id:req.params.id};
    if (req.params.version != undefined){
      query =  {...query, version:req.params.version};
    }
    
    var b = Burnup.find(query).sort({ version: -1 }).limit(1)
    .then(function(docs){
      
      if (docs.length ===1){
        res.status(200).json(docs[0]);
      } else {
      
      res.sendStatus(404);
      }
    }, function(err){
      console.log('errored');
      res.sendStatus(404);
    });
  
});

//save
router.post('/', function(req, res){
  //generate id
  var id = shortid.generate();
  //loop while doesnt exist (so not to create duplicate)
  var toSave = {...req.body,id:id, version:1};
  var burnup = new Burnup(toSave);
  burnup.save(function(err) {
    if (err) {
      console.log(err.errors);
      throw err};

    console.log('Burnup created!');
    res.status(201).json(burnup);
  });
 //write to DB
});
router.put('/:id', function(req, res){
  //get latest and up version
  console.log('put');
  var query = {id:req.params.id};
  //save new doc with new version 
  var b = Burnup.find(query).sort({ version: -1 }).limit(1)
    .then(function(docs){
      
        if (docs.length ===1){
          var toUpdate = req.body;
          delete toUpdate['_id'];
          delete toUpdate['_v'];
          toUpdate.version  = docs[0].version+1;
          console.log(toUpdate);
        
          var burnup = new Burnup(toUpdate);
          burnup.save(function(err) {
            if (err) {
              console.log(err.errors);
              throw err};

            console.log('Burnup created!');
            res.status(201).json(burnup);
        });
      }
    });
  
})


export default router;