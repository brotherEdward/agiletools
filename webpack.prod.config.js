var path = require('path');
const webpack = require('webpack');
const webpackIsomorphicToolsConfig = require('./webpack-isomorphic-tools');
const WebpackIsomorphicToolsPlugin = require('webpack-isomorphic-tools/plugin');
const webpackIsomorphicToolsPlugin = new WebpackIsomorphicToolsPlugin(webpackIsomorphicToolsConfig);
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const autoprefixer = require('autoprefixer');
var outDirectory = (process.env.NODE_ENV === 'production') ?
  'dist' :
  'build';

module.exports = {
  entry: [
    './client'
  ],

  output: {
    path:     path.join(__dirname, outDirectory),
    filename: 'bundle.js',

  },
  resolve: {
    modules: ['node_modules', 'shared'],
    extensions:[ '.js', '.jsx', '.css']
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        loader:   ExtractTextPlugin.extract({
              fallbackLoader: 'style-loader',
              loader: [
                 {
                  loader: 'css-loader',
                   options: {
                    modules: true,
                    importLoaders:2,
                    sourceMap:true,
                    localIdentName:'/[local]___[hash:base64:5]'
                   }
                 },
                {
                  loader: 'postcss-loader'
                 }
            ]
        }) 
      },
       {
        test: webpackIsomorphicToolsPlugin.regular_expression('images'),
        loader: ['url-loader?limit=10240&name=/img/[name].[ext]'],
      },
      {
        test:    /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      }
     
    ]
  },
 

  plugins: [
     webpackIsomorphicToolsPlugin,
    new ExtractTextPlugin({filename:'/[name]-[chunkhash].css', allChunks: true }),
  
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"',
      },

      __CLIENT__: true,
      __DEVTOOLS__: false,
    }),

   
  ],
};