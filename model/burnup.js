var mongoose = require('mongoose');  
var burnupSchema = new mongoose.Schema({  
  id: String,
  version:Number,
  teamName: String,
  sprints: [{ number: Number, points: Number, totalPoints: Number }],
});
var Burnup = mongoose.model('Burnup', burnupSchema);

module.exports = Burnup;