import express                   from 'express';
import React                     from 'react';
import path                      from 'path';
import { renderToString, renderToStaticMarkup }        from 'react-dom/server'
import { createStore, combineReducers } from 'redux';
import { Provider }                     from 'react-redux';
import * as reducers             from 'reducers';
import * as serverReducers             from 'reducers/serverOnly';
import { applyMiddleware }       from 'redux';
import promiseMiddleware         from 'lib/promiseMiddleware';
import { StaticRouter } from 'react-router'
import Default from './shared/layouts/default';
import AgileTools from './shared/components';
import burnupRouter from './api/burnup';


const app = express();

app.use(express.static(path.join(__dirname, 'dist')));

app.use('/api/burnup', burnupRouter);

app.get('/*', function (req, res) {
  
  if (process.env.NODE_ENV === 'development') {
    webpackIsomorphicTools.refresh();
  }

  const reducer  = combineReducers({...reducers, ...serverReducers});
  const store = applyMiddleware(promiseMiddleware)(createStore)(reducer);
  if (req.url) {
    const context = {};
    const server = (
      <StaticRouter location={req.url} context={context}>
        <Provider store={store}>
          <AgileTools/>
        </Provider>
      </StaticRouter>
    );
    var html = renderToString(server);
    
    const redirect = context.url;

    if (redirect) return res.redirect(redirect.pathname)
    
    return Promise.all(store.getState().promises)
    .then(() => {
      //re-render 
       const markup = renderToString(server);
       var state = store.getState();
       console.log('state');
       console.log(state);
       //remove server side state
       delete state.promises;
       const wrapped = `
          <!DOCTYPE html>
          ${renderToStaticMarkup(<Default assets={webpackIsomorphicTools.assets()} component={markup} state={state} />)}
          `;
        res.status(200)
        .end(wrapped);
        })
    .catch((e) => {
        var markup ="";
        console.error("error - " + e)
        var state = store.getState();
        
        //remove server side state
        delete state.promises;
             
        const wrapped = `
            <!DOCTYPE html>
            ${renderToStaticMarkup(<Default assets={webpackIsomorphicTools.assets()} component={markup} state={state} />)}
            `;
                 
        res.status(404).end(wrapped);
    })
  }
});

const PORT = process.env.PORT || 3000;

app.listen(PORT, function () {
  console.log('Server listening on', PORT);
});
export default app;