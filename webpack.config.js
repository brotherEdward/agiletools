var path    = require('path');
var webpack = require('webpack');
const webpackIsomorphicToolsConfig = require('./webpack-isomorphic-tools');
const WebpackIsomorphicToolsPlugin = require('webpack-isomorphic-tools/plugin');
const webpackIsomorphicToolsPlugin = new WebpackIsomorphicToolsPlugin(webpackIsomorphicToolsConfig);
const autoprefixer = require('autoprefixer');





module.exports = {
  entry:  [
    'webpack-dev-server/client?http://localhost:8080/',
    'webpack/hot/only-dev-server',
    './client/index.jsx'
  ],
  output: {
    path:     path.join(__dirname, 'dist'),
    filename: 'bundle.js',
    publicPath: 'http://localhost:8080/'
  },
  resolve: {
    modules: ['node_modules', 'shared'],
    extensions:[ '.js', '.jsx', '.css']
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [{
                  loader: 'style-loader'
                 },
                 {
                  loader: 'css-loader',
                   options: {
                    modules: true, 
                    importLoaders:1,
                    sourceMap:true, 
                    localIdentName:'/[local]___[hash:base64:5]'
                   }
                 },
                 {
                  loader: 'postcss-loader'
                 }
              ]
      
      },
       {
        test: webpackIsomorphicToolsPlugin.regular_expression('images'),
        loader: ['url-loader?limit=10240&name=/img/[name].[ext]'],
      },
      {
        test:    /\.jsx?$/,
        exclude: /node_modules/,
        use: ['react-hot-loader', 'babel-loader']
      }
     
    ]
  },

  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
     new webpack.DefinePlugin({
      __CLIENT__: true,
      __DEVTOOLS__: true,
      'process.env': {
        NODE_ENV: '"development"',
      },
    }),
    webpackIsomorphicToolsPlugin.development(),
  ],
  devtool: 'inline-source-map',
  devServer: {
    hot: true,
    proxy: {
      '*': 'http://127.0.0.1:' + (process.env.PORT || 3000)
    },
    host: '127.0.0.1'
  }
};