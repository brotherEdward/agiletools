import React from 'react';
import ReactDOM from 'react-dom/server';
import mutifyState   from '../lib/mutifyState';
import { toJS } from 'immutable';

export default class Default extends React.Component {
  render() {
    const { assets, content, state } = this.props;
    const muteState = mutifyState(state);
  
    var stateSerialized = JSON.stringify(muteState);
    var wrap = (
      <html lang="en">
        <head>
          <meta charSet="utf-8" />
          <title>Agile Tools</title>
         
          {/* link any webfonts here */}
          <link href="https://fonts.googleapis.com/css?family=Overpass" rel="stylesheet" />

          <link rel="stylesheet" href="https://cdn.rawgit.com/novus/nvd3/v1.8.1/build/nv.d3.css" />
          {/* production */}
          {Object.keys(assets.styles).map((style, key) =>
            <link
              href={assets.styles[style]}
              key={key} media="screen, projection"
              rel="stylesheet" type="text/css" charSet="UTF-8"
            />
          )}
        </head>
        <body>
          <div id="react-view" dangerouslySetInnerHTML={{ __html: content }} />
          <script
            dangerouslySetInnerHTML={{ __html: `window.__INITIAL_STATE__=${stateSerialized};` }}
            charSet="UTF-8"
          />
          <script
            src={
              process.env.NODE_ENV === 'development' ?
              `http://127.0.0.1:8080/bundle.js` :
              '/bundle.js'
            }
            charSet="UTF-8"
          />
        </body>
      </html>
    );
    return wrap;
  }
}

Default.propTypes = {
  assets: React.PropTypes.object,
  component: React.PropTypes.node,
  state: React.PropTypes.object,
};