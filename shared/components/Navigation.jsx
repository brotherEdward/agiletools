import React from 'react';
import styles from './nav.css';
import { Link } from 'react-router-dom';

class Navigation extends React.Component {
   handleSubmit = () => {
    this.props.addSprint();
  }
  render() {
    
    return (
         <nav className={styles.navcollapse}>
        <ul className={styles.nav}>

            <li><Link to="/">Home</Link></li>
            <li><Link to="/Burnup">Burnup</Link></li>
            <li><Link to="/Principles">Principles</Link></li>
            <li><Link to="/Calendar">Calendar</Link></li>

            </ul>
        </nav>

  ); 
  }
}
export default Navigation;