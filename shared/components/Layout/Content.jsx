import React from 'react';

import transCss from './trans.css';


class Content extends React.Component {
  
  
  render() {
    
    return (
        <div id='content-pusher' onClick={this.props.hideMenu} className={transCss.stPusher}>
            <div className={transCss.stContent}>
              <div className={transCss.stContentInner}>
                  {this.props.children}
                </div>
            </div>
          </div>
    ); 
  }
}
export default Content;