import React from 'react';
import sprintImage from '../../../assets/sprint1.png';
import saveImage from '../../../assets/save.png';
import transCss from './trans.css';
import styles from './menu.css';

class SideBar extends React.Component {
  
  
  render() {
    
    return (
        <div className={transCss.stMenu + ' ' + transCss.stEffect} id="sidebar">
            <div id="menu" className={styles.menu2} >
              <div id='show-menu-btn' className={styles.menuButton } onClick={this.props.showMenu}>
                <img src={sprintImage}/>
              </div>
              <div id='save-btn' className={styles.menuButton +' '+styles.saveButton} onClick={this.props.save} >
                <img src={saveImage}/>
              </div>
            </div>
            {this.props.children}
          </div>
    ); 
  }
}
export default SideBar;