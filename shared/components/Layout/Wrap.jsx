import React from 'react';

import transCss from './trans.css';
import styles from './wrap.css';


class Wrap extends React.Component {
  
  render() {
    
    return (
        <div>
            <header className={styles.sectionHead}>{this.props.title}</header>
            <div id="st-container" className={transCss.stContainer + ' '+ transCss.stEffect + ' ' + (this.props.menuOpen ? transCss.stMenuOpen : '')  }>
                {this.props.children}
            </div>
        </div>
    ); 
  }
}
export default Wrap;