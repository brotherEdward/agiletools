import React from 'react';
import styles from './header.css';

class Header extends React.Component {
   handleSubmit = () => {
    this.props.addSprint();
  }
  render() {
    
    return (
        <div>
        <div className={styles.headerFrameWrap}>
            <div className={styles.headerFrame}></div>
        </div>
        <header className={styles.mainheader}>
            <div className={styles.mainheadercontent}>
                <div className={styles.logo}>
                    <div className={styles.logoContent}>
                    <a href="#home">a<strong>g</strong>ile tools</a>
                    </div>
                </div>
                {this.props.children}
            </div>    
        </header>
        </div>
  ); 
  }
}
export default Header;