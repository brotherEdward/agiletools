import React from 'react';
import styles from './stat.css';

class Stat extends React.Component {
  

  render() {
    
    return (
         <div className={styles.stat}>
            <div className={styles.value}>{this.props.value}</div>
            <div className={styles.label}>{this.props.label}</div>
          </div>
    ); 
  }
}
export default Stat;