import React from 'react';
import * as SprintActions       from '../../actions/sprintActions';
import styles from './sprintList.css';
import closeImage from '../../../assets/close.png';
import AddSprint              from './AddSprint';
import buttonStyle from './addsprint.css';

class SprintList extends React.Component {  
  updatePoints= (e) => {
    this.props.updatePoints(e.target.dataset.index, parseInt(e.target.value));
  }
  updateTotalPoints = (e)=>{
    this.props.updateTotalPoints(e.target.dataset.index, parseInt(e.target.value));
  }
   handleRemoveSprint = () => {
    this.props.removeSprint();
  }
  render() {
     const { sprints, addSprint} = this.props;
    return (
      <div className={styles.sprintList} >
      <table>
      <thead>
        <tr>
        <th>sprint</th>
        <th>total</th>
        <th>done done</th>
        </tr>
      </thead>
      <tbody>
        {
          sprints.map( (sprint, index) => {   
            return (
              <tr key={sprint.get("number")}>
                <td>{sprint.get("number")}</td> 
                <td><input type='number' onChange={this.updateTotalPoints} data-index={index} value={sprint.get("totalPoints")}/></td>
                <td><input type='number' onChange={this.updatePoints} data-index={index} value={sprint.get("points")}/> </td>
                <td>{index == sprints.size-1 && <img className={buttonStyle.addSprintButton} src={closeImage} onClick={this.handleRemoveSprint}/>}</td>
              </tr>
              //<SprintRow sprint={sprint} {...bindActionCreators(SprintActions, dispatch)} />
            );
          })
        }
        <tr>
        <td> <AddSprint addSprint={addSprint} /> </td>
        </tr>
        </tbody>
        </table>
      
      </div>

    );
  }
}

export default  SprintList;