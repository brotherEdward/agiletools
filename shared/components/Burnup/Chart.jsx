import React from 'react';
import NVD3Chart from 'react-nvd3';
import styles from './chart.css';

class Chart extends React.Component {
  

  render() {
    
    return (
      <div id='chart' className={styles.chart}>
        <NVD3Chart id="lineChart" type="lineChart" datum={this.props.datum} x="Sprint" y="Points"  
          xAxis={{ axisLabel: "Sprint", showMaxMin: false}} 
          yAxis={{ axisLabel: "Points", showMaxMin: false }} 
          margin={{right:50}} 
        />
      </div>
    ); 
  }
}
export default Chart;