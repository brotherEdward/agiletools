import React                  from 'react';
import SprintList             from './SprintList';
import Chart               from './Chart';
import Stat               from './Stat';
import SideBar             from '../Layout/SideBar';  
import Content             from '../Layout/Content';
import Wrap             from '../Layout/Wrap';
import { bindActionCreators } from 'redux';
import * as sprintActions     from '../../actions/sprintActions';
import * as viewActions     from '../../actions/viewActions';
import { connect }            from 'react-redux';
import styles from './burnup.css';
import mainCss from '../main.css';
import chartStyles from './chart.css';
import Satisfy from 'react-satisfy';
import NVD3Chart from 'react-nvd3';
import {range} from 'lodash';
import d3 from 'd3';
import stdDeviation from  '../../lib/stdDeviation';
import predictVelocity from  '../../lib/predictVelocity';
import { withRouter } from 'react-router'


function mapDispatchToProps(dispatch) {
  return { actions: bindActionCreators(sprintActions, dispatch),
  viewActions: bindActionCreators(viewActions, dispatch)
   }
}


class Burnup extends React.Component {
 
 
  handleSave = (e) => {
    this.props.actions.save(this.props.burnup);
  }

  handleShowMenu = (e) => {
    e.preventDefault();
    if (this.props.menuOpen) {
      this.props.viewActions.hideMenu();  
    }
    else {
     this.props.viewActions.showMenu();
    }
  }
  handleHideMenu = (e) => {
    if (this.props.menuOpen){
      this.props.viewActions.hideMenu();  
    }
  }

  render() {
    const { burnup, menuOpen, actions, match } = this.props;
    const params = match.params;
    var riskProfile ={'name':'High', 'ten':1, 'fifty':1.4, 'ninety':1.8};
    var sprints =  burnup.get("sprints");
    var shouldLoad = !params || params.id===undefined || sprints.size!=0;
    var datum = [];

    var id = params? params.id : undefined;
    var version = params? params.version : undefined;
    if (sprints.size >0)
    {
      var sprintsWithPoints = isNaN(sprints.last().get("points")) ? sprints.pop() : sprints;
      var sprintsWithTotals = isNaN(sprints.last().get("totalPoints")) ? sprints.pop() : sprints;

      var backlog = sprintsWithTotals.map(s=>{return {x:s.get("number"),y:s.get("totalPoints")}  }).toArray();
      
      datum.push({ key:'Backlog', values: backlog,color:'#005999'},);

      var noSprintsCompleted = sprintsWithPoints.size;
      var tPointsSize = sprintsWithTotals.size;

      if (noSprintsCompleted < 3) {
        riskProfile ={'name':'Low', 'ten':1, 'fifty':2, 'ninety':4};
      }
      var sprintpointsonly = sprintsWithPoints.map(s=>s.get("points")).toArray();
      
      var stdD = stdDeviation(sprintpointsonly).toFixed(2);

      var cumulativeCompleted  = sprintsWithPoints.reduce(function(r, s) { r.push({x:s.get("number"),y:((r.length && r[r.length - 1].y || 0) + s.get("points"))});return r;}, []);
      
      datum.push({ key:'Points Delivered', values: cumulativeCompleted,color:'#fcc438'},);

      if ((noSprintsCompleted ||tPointsSize  >0) && cumulativeCompleted.length>0){
        var totalCompleted =cumulativeCompleted[cumulativeCompleted.length-1].y;
 
        var average = Math.floor(totalCompleted / noSprintsCompleted);
        var left = sprintsWithTotals.last().get("totalPoints")- totalCompleted;
        var ten = predictVelocity(noSprintsCompleted, average, totalCompleted, left, riskProfile.ten);
        var fifty = predictVelocity(noSprintsCompleted, average, totalCompleted, left, riskProfile.fifty);
        var ninety = predictVelocity(noSprintsCompleted, average, totalCompleted, left, riskProfile.ninety);
      
        if (noSprintsCompleted<ninety.totalSprintsRequired){
          datum.push( { key:'10%', values: ten.cumulative, classed: chartStyles.dashed, color:'#fcc438'});
          datum.push( { key:'50%', values: fifty.cumulative, classed: chartStyles.dashed, color:'#fcc438'});
          datum.push( { key:'90%', values: ninety.cumulative, classed: chartStyles.dashed, color:'#fcc438' });
        }
        if (tPointsSize < ninety.totalSprintsRequired){
          var predTotal = range(tPointsSize,ninety.totalSprintsRequired+1).map(i=>{return {x:i,y:sprintsWithTotals.last().get("totalPoints")}});          
          datum.push( { key:'Predicrted Total', values: predTotal, color:'#005999', classed: chartStyles.dashed },);
        }
        
      }
    }
    return (
      <div>
        <Satisfy condition={shouldLoad} action={ actions.getSprints } id={ id } version={version} />
        
        <div className={styles.banner}>
          <Stat value={totalCompleted} label="done done" />
          <Stat value={average} label="average points" />
          <Stat value={ten ? ten.totalSprintsRequired : ''} label="10% chance" />
          <Stat value={fifty ? fifty.totalSprintsRequired : ''} label="50% chance" />
          <Stat value={ninety ? ninety.totalSprintsRequired : ''} label="90% chance" />
          <Stat value={riskProfile.name} label="confidence profile" />
          <Stat value={stdD} label="standard deviation" />
        </div>
        <Wrap title={'Burn up'} menuOpen={menuOpen} >
          <SideBar save={this.handleSave} showMenu={this.handleShowMenu}>
              <SprintList sprints={sprints} addSprint={actions.addSprint} removeSprint={actions.removeSprint} updatePoints={actions.updatePoints} updateTotalPoints={actions.updateTotalPoints} />
          </SideBar>
          <Content hideMenu={this.handleHideMenu}>
            <Chart datum={datum} />
          </Content>
        </Wrap>
      </div>
    );
  }

}
export default withRouter(connect(state => ({ burnup: state.burnup, menuOpen:state.view.get("showMenu")}),
mapDispatchToProps)(Burnup));