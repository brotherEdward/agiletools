import React from 'react';
import addImage from '../../../assets/addblk.png';
import styles from './addsprint.css';

class AddSprint extends React.Component {
  
   handleSubmit = () => {
    this.props.addSprint();
  }
  render() {
    
    return (
        <img className={styles.addSprintButton} src={addImage} onClick={this.handleSubmit} />
    ); 
  }
}
export default AddSprint;