import React, { Component, PropTypes } from 'react';
import { Route,Switch, Redirect } from 'react-router';
import NoMatch from './NoMatch';
import Home from  './Home';
import Principles from  './Principles';
import Burnup from  './Burnup';
import Calendar from  './Calendar';
import Header from  './Header';
import Navigation from  './Navigation';
import styles from './main.css';
import { connect }            from 'react-redux';
import { withRouter } from 'react-router'



class  App extends Component {
  render() {
    const id = this.props.burnup ? this.props.burnup.get("id") : undefined;
    const version = this.props.burnup ? this.props.burnup.get("version") : undefined;

    return (
      <div>
      <Header >
        <Navigation/>
      </Header>
        
        <div className={styles.content}>
         <Switch>
          
          <Route exact path="/Burnup/:id/:version" render={(matchProps) => (
              id && version && version!=matchProps.match.params.version ? ( <Redirect  to={'/Burnup/'+id+'/'+version} />) : 
                (this.props.burnup ?  ( <Burnup {...matchProps}/>) :(<NoMatch /> ))) 
          }/>
          <Route exact path="/Burnup/:id" render={(matchProps) => (
          this.props.burnup ?  ( <Burnup {...matchProps}/>) :(<NoMatch /> ))}
          />
          <Route exact path="/Burnup" render={(matchProps) => (
              id && version ? ( <Redirect  to={'/Burnup/'+id+'/'+version} /> ) : (  <Burnup {...matchProps}/>)
          )}/>
          <Route exact path="/Principles" component={Principles} />
          <Route exact path="/Calendar" component={Calendar} />
          <Route exact path="/"component={Home}/>
          <Route component={NoMatch} />
          </Switch>
        </div>
      </div>
    )
  }
}

export default withRouter(connect(state => ({ burnup: state.burnup}))(App));