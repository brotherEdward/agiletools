import {range} from 'lodash';

function predictVelocity(noSprints, averagePoints, totalCompleted, remainingPoints, riskFactor){
    var factoredAverage = Math.floor(averagePoints/riskFactor);
    //avoid divide by 0 :)
    factoredAverage = factoredAverage === 0 ? 1 : factoredAverage;
    var predictedTotalSprints = Math.ceil(remainingPoints/factoredAverage) + noSprints;

    var predictedRange = range(noSprints+1,predictedTotalSprints+1).map(i=>{return {x:i,y:factoredAverage}});
    predictedRange.unshift({x:noSprints, y:totalCompleted});
    var cumulative  = predictedRange.reduce(function(r, s) { r.push({x:s.x,y:((r.length && r[r.length - 1].y || 0) + s.y)});return r;}, []);
    return {'cumulative':cumulative, 'totalSprintsRequired':predictedTotalSprints, 'factoredAverage':factoredAverage};
}


export default predictVelocity;