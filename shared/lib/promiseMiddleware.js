import { awaitPromise } from '../actions/promiseActions'

export default () => (next) => (action) => {
  const { promise, type, ...rest } = action

  if (!promise) return next(action)
  const SUCCESS = type;
  const REQUEST = type + '_REQUEST';
  const FAILURE = type + '_FAILURE';

  const actionPromise = promise
  actionPromise.then(
    (result) => next({ ...rest, result, type: SUCCESS }),
    (error) => next({ ...rest, error, type: FAILURE })
  ).catch((error) => {
    // TODO logger
    console.error('MIDDLEWARE ERROR:', error) // eslint-disable-line
    next({ ...rest, error, type: FAILURE })
  })

  next(awaitPromise(actionPromise))
  return actionPromise
}