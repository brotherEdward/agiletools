import { toJS } from 'immutable';

export default function immutifyState(obj) {
  let objMut = {...obj};

  Object
    .keys(objMut)
    .forEach(key => {
      objMut[key] = objMut[key].toJS();
    });

  return objMut;
}