import Immutable from 'immutable';
import { fromJS } from 'immutable';
import {ADD_SPRINT, REMOVE_SPRINT, UPDATE_POINTS, UPDATE_TOTAL_POINTS, SAVE_BURNUP, GET_SPRINTS,GET_SPRINTS_FAILURE,SPRINTS_ERROR} from '../constants';
const defaultState = fromJS({ sprints:[]});

export default function sprintReducer(state = defaultState, action) {
  switch(action.type) {
    case ADD_SPRINT:
    {
      var size = state.get("sprints").size ;
      var nextSprint = size === 0 || size === undefined ? 1 : size +1;
      var lastTotal = state.get("sprints").size>0 ? state.get("sprints").get(size-1).get("totalPoints") : 0 ;
      return state.set("sprints", state.get("sprints").push(Immutable.Map({'number':nextSprint, 'points': 0, 'totalPoints':lastTotal})));
    }
    case REMOVE_SPRINT:
    {
      return state.set("sprints", state.get("sprints").pop());
    }
    case SAVE_BURNUP:
    {
      return fromJS(action.result.data);
    }
    case UPDATE_POINTS:
    {   
        return state.set("sprints", state.get("sprints").update(action.index, s=>       
          s.set("points",action.value)));
    }
    case UPDATE_TOTAL_POINTS:
    {
        return state.set("sprints", state.get("sprints").update(action.index, s=>       
          s.set("totalPoints",action.value)));
    }
    case GET_SPRINTS:
    { 
      return fromJS(action.result.data);
    }
    case GET_SPRINTS_FAILURE:
    {
      console.log('result not found');
      return null;
    }
    case SPRINTS_ERROR:
      return state;
    default:
      return state;
  }
};

