import Immutable from 'immutable';
const defaultState = new Immutable.List();
export default function promisesReducer(state = defaultState, action) {
  switch(action.type) {
    case 'AWAIT_PROMISE':
      return process.browser ? state :  [...state, action.promise];
    default:
      return state;
  }
};

