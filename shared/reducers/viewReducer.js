import Immutable from 'immutable';
import { fromJS } from 'immutable';
import {SHOW_MENU, HIDE_MENU} from '../constants';
const defaultState =fromJS({showMenu:true});
export default function viewReducer(state = defaultState, action) {
  switch(action.type) {
    case SHOW_MENU:
      {
       return state.set("showMenu", true);
      }
      case HIDE_MENU:
      {
        return state.set("showMenu", false); 
      }
    default:
      return state;
  }
};

