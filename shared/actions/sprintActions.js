import request from 'axios';
import {ADD_SPRINT, REMOVE_SPRINT, UPDATE_POINTS, UPDATE_TOTAL_POINTS, SAVE_BURNUP, GET_SPRINTS} from '../constants';
const BACKEND_URL = 'http://localhost:3000/api/burnup';


export function addSprint() {
  return {
    type: ADD_SPRINT
  }
}
export function removeSprint() {
  return {
    type: REMOVE_SPRINT
  }
}
export function updatePoints(index, value) {
  return {
    type: UPDATE_POINTS,
    index: index, 
    value:value
  }
}
export function updateTotalPoints(index, value) {
  return {
    type: UPDATE_TOTAL_POINTS,
    index: index, 
    value:value
  }
}
export function getSprints(params) {
  let url = BACKEND_URL + "/"+params.id;
  console.log('getting - '+url);
  if (params.version)
    url = url + "/" + params.version;  
  return {
    type: GET_SPRINTS,
    promise: request.get(url)
  }
}

export function save(burnup) {
  //need toJS this bad boy
  var converted = burnup.toJS();
  if (converted.id === undefined){
    return {
      type: SAVE_BURNUP,
      promise: request.post(BACKEND_URL, converted)
    }
  } else {
    var url = BACKEND_URL + '/'+converted.id;
    return {
      type: SAVE_BURNUP,
      promise: request.put(url, converted)
    }
  }
}
